import axios from 'axios';

export class BooksApi {
    static async getAllBooks() {
        try {
            const booksReq = await axios.get('/api/books');
            return booksReq.data;
        }
        catch (err) {
            console.error('there was an error accessing api');
        }
    }
}