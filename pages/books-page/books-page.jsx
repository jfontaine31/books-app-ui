import React, { Component } from 'react';
import { BooksApi } from "../../api/books-api.js";

import { BooksList } from "../../components/books-list/books-list.jsx";

export class BooksPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            books: []
        }

    }

    async componentDidMount() {
        try {
            const { data :books } = await BooksApi.getAllBooks();
            this.setState({ books });
        }
        catch (err) {
            console.error("error getting books");
        }
    }

    render() {
        const { books } = this.state;
        return (
          <div className="books-page">
              <BooksList books={books} />
          </div>
        );
    }

}