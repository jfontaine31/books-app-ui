const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ProvidePlugin } = require('webpack');
const path = require('path');

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: "build.[name].[hash].js",
        path: path.join(__dirname, 'dist'),
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'less-loader',
                        options: {
                            sourceMap: true,
                        },
                    }
                ]
            }
        ]
    },
    devServer: {
        port: 3000,
        open: false,
        historyApiFallback: true,
        proxy: {
            '/api': {
                target: "http://localhost:8000",
                changeOrigin: true,
            },
        }
    },
    watch: true,
    devtool: "inline-source-map",
    plugins: [
        new HtmlWebpackPlugin({
            title: "Books App",
            template: 'src/index.html'
        }),
        new ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Tether: 'tether'
        })
    ],
};
