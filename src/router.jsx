import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch,  } from "react-router-dom";

import { HelloWorld } from "../components/hello-world/hello-world.jsx";
import { BooksPage } from "../pages/books-page/books-page.jsx";

export class AppRouter extends Component {
    render() {
        return (
            <div>
                <Router>
                    <div className="navbar">
                        <ul>
                            <li>
                                <Link to="/">Hello World Example</Link></li>
                            <li>
                                <Link to="/books">
                                    Books DB
                                </Link>

                            </li>
                        </ul>
                    </div>
                    <hr/>
                    <Switch>
                        <Route exact path="/" component={HelloWorld}/>
                        <Route path="/books" component={BooksPage} />
                    </Switch>
                </Router>
            </div>
        );
    }
}