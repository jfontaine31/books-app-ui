import React from 'react';
import ReactDOM from 'react-dom';
import { AppRouter } from "./router.jsx";

import "bootstrap"; // bootstrap install
import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
    <AppRouter />,
    document.getElementById("app")
);
