import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { BookItem } from "../book-item/book-item.jsx";

export class BooksList extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const { books = [] } = this.props;
        return (
          <div className="books-list">
              <div className="friend">List of Books Read</div>
              <ul className="list">
                  {
                      books.map(book => (
                          <BookItem key={book.id} title={book.title} author={book.author}/>
                      ))
                  }
              </ul>
          </div>
        );
    }
}

BooksList.propTypes = {
    books: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        author: PropTypes.string.isRequired
    })).isRequired
};
