import React from 'react';
import PropTypes from 'prop-types';

export const BookItem = (props) => {
    const { title, author } = props;
    return (
        <li>
            <span className="title">{title}</span> -
            <span className="author">{author}</span>
        </li>
    );
};

BookItem.propTypes = {
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired
};

