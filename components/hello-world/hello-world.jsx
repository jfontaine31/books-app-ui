import React from 'react';

import "./hello-world.less";

export class HelloWorld extends React.Component {
    render() {
        return (
            <div className="jumbotron col-sm-6 col-offset-3 hello-world">
                Hello World
            </div>
        );
    }
}